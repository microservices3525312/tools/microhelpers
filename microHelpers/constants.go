package helperTools

import (
	"github.com/go-playground/validator/v10"
	"github.com/microcosm-cc/bluemonday"
)

var (
	policy *bluemonday.Policy
)

type Validator struct {
	validator *validator.Validate
}
