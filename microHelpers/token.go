package helperTools

import (
	"fmt"
	"strconv"
	"time"

	"github.com/golang-jwt/jwt"
)

func GenerateToken(userID string, expiresIn string, tokenType string, key []byte) (string, error) {
	exp, err := strconv.Atoi(expiresIn)

	if err != nil {
		return "No Auth", err
	}

	claims := jwt.MapClaims{
		"userID":    userID,
		"tokenType": tokenType,
		"exp":       time.Now().Add(time.Duration(exp) * time.Minute).Unix(),
	}

	return jwt.NewWithClaims(jwt.SigningMethodHS256, claims).SignedString(key)
}

func VerifyToken(token string, key []byte) (*jwt.Token, error) {
	return jwt.Parse(token, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}
		return key, nil
	})
}
