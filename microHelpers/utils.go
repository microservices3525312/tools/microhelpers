package helperTools

import (
	"crypto/rand"
	"errors"
	"fmt"
	"math/big"
	"net"
	"os"
	"regexp"
	"time"
)

func GetIPv6() (result string) {
	host, _ := os.Hostname()
	address, _ := net.LookupIP(host)

	for _, a := range address {
		if ipv6 := a.To16(); ipv6 != nil {
			result = ipv6.String()
			break
		}
	}

	return result
}

func GetFormattedDateTime() string {
	return time.Now().Format("2006-01-02 15:04")
}

func GenerateValidationStruct(e error) error {
	fieldNames := make([]string, 0, 0)
	re := regexp.MustCompile(`'([^']*)' failed`)
	matches := re.FindAllStringSubmatch(e.Error(), -1)

	for _, match := range matches {
		fieldNames = append(fieldNames, match[1])
	}

	return errors.New(fmt.Sprintf("verifications failed for fields: %v", fieldNames))
}

func OTP() (string, error) {
	min := big.NewInt(100_000_000_000)
	max := big.NewInt(999_999_999_999)

	rangeInt := big.NewInt(0).Sub(max, min)
	rangeInt.Add(rangeInt, big.NewInt(1))

	randomInt, err := rand.Int(rand.Reader, rangeInt)
	if err != nil {
		return "", err
	}

	randomInt.Add(randomInt, min)

	otp := fmt.Sprintf("%03d %03d %03d %03d",
		randomInt.Uint64()/1_000_000_000,
		(randomInt.Uint64()%1_000_000_000)/1_000_000,
		(randomInt.Uint64()%1_000_000)/1_000,
		randomInt.Uint64()%1_000)

	return otp, nil
}
