package helperTools

import (
	"crypto/rand"
	"encoding/base64"
	"encoding/hex"
	"errors"
	"fmt"
	"reflect"
	"sync"
	"unicode/utf8"

	"golang.org/x/crypto/argon2"
)

func Hash(password, salt string) (string, error) {
	hash := argon2.IDKey([]byte(password), []byte(salt), 1, 64*1024, 4, 32)
	if hash == nil {
		return "", errors.New("failed to generate hash")
	}

	hashRunes := []rune(hex.EncodeToString(hash))

	var wg sync.WaitGroup
	for i := 0; i < len(hashRunes); i++ {
		wg.Add(1)
		go func(i int) {
			// Replace any invalid UTF-8 characters with a placeholder
			if !utf8.ValidRune(hashRunes[i]) {
				hashRunes[i] = '�'
			}
			wg.Done()
		}(i)
	}
	wg.Wait()

	// Convert the slice of runes back to a string
	hashString := string(hashRunes)
	return hashString, nil

}

func Salt(length int8) (string, error) {
	saltBytes := make([]byte, length)
	_, err := rand.Read(saltBytes)
	if err != nil {
		return "", err
	}

	encodedSalt := base64.RawStdEncoding.EncodeToString(saltBytes)
	return encodedSalt, nil
}

func XSSInput(s interface{}, pm *PolicyManager) error {
	val := reflect.ValueOf(s)

	if val.Kind() != reflect.Ptr || val.Elem().Kind() != reflect.Struct {
		return errors.New("expected a pointer to a struct")
	}

	val = val.Elem()

	var wg sync.WaitGroup
	for i := 0; i < val.NumField(); i++ {
		field := val.Field(i)
		if field.Kind() == reflect.String {
			wg.Add(1)
			go func(f reflect.Value) {
				defer wg.Done()
				f.SetString(pm.GetPolicy().Sanitize(f.String()))
			}(field)
		}
	}

	wg.Wait()
	return nil
}

func LimitReqBody(body []byte, maxReqBodySize int) error {
	if len(body) > maxReqBodySize {
		return fmt.Errorf("[Request Body Size Middleware] - Request Body Is Too Large]. Your Request Size: %v, Maximum Allowed Size: %v", len(body), maxReqBodySize)
	}

	return nil
}
