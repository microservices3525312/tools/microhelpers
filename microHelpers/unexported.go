package helperTools

import (
	"sync"

	"github.com/microcosm-cc/bluemonday"
)

func stringSanitizer(input string) string {
	return bluemonday.UGCPolicy().Sanitize(input)
}

func intSanitizer(input string) string {
	return bluemonday.UGCPolicy().Sanitize(input)
}

func initPolicy() {
	policy = bluemonday.UGCPolicy()
}

type PolicyManager struct {
	policy     *bluemonday.Policy
	policyOnce sync.Once
}

func (pm *PolicyManager) initPolicy() {
	pm.policy = bluemonday.UGCPolicy()
}

func (pm *PolicyManager) GetPolicy() *bluemonday.Policy {
	pm.policyOnce.Do(pm.initPolicy)
	return pm.policy
}
