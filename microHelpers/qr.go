package helperTools

import (
	"image/color"

	"github.com/skip2/go-qrcode"
)

type QR struct {
	Url, FileName    string
	Size             int
	Level            qrcode.RecoveryLevel
	BgColor, FgColor color.RGBA
}

func (q *QR) Qr() error {
	return qrcode.WriteFile(q.Url, q.Level, q.Size, q.FileName)
}

func (q *QR) QrColor() error {
	return qrcode.WriteColorFile(q.Url, q.Level, q.Size, q.BgColor, q.FgColor, q.FileName)
}
