module gitlab.com/microservices3525312/tools/microhelpers

go 1.20

require (
	github.com/go-playground/validator/v10 v10.12.0
	github.com/microcosm-cc/bluemonday v1.0.23
	github.com/skip2/go-qrcode v0.0.0-20200617195104-da1b6568686e
	golang.org/x/crypto v0.8.0
)

require (
	github.com/aymerick/douceur v0.2.0 // indirect
	github.com/go-playground/locales v0.14.1 // indirect
	github.com/go-playground/universal-translator v0.18.1 // indirect
	github.com/golang-jwt/jwt v3.2.2+incompatible // indirect
	github.com/gorilla/css v1.0.0 // indirect
	github.com/leodido/go-urn v1.2.3 // indirect
	golang.org/x/net v0.9.0 // indirect
	golang.org/x/sys v0.7.0 // indirect
	golang.org/x/text v0.9.0 // indirect
)
